
public class konsolenausgabe {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		
		
		// "Hallo Welt" ausgeben 
		System.out.print("Hallo Welt\r");
		// "Hallo Welt" ausgeben und einen Zeilenvorschub ausf�hren 
		System.out.println("Hallo Welt\n");
		
		// Ausgabe: Er sagte: "Guten Tag!"
		System.out.println("Er sagte: \"Guten Tag!\n");  
		
		// "Ich hei�e Willi und bin 56 Jahre alt." ausgeben
int alter = 56;
String name = "Willi";
		
		System.out.print("Ich hei�e " + name + " und bin " + alter + " Jahre alt.");



		System.out.println("\n");
	
		
String s = "Java-Programm";
		
		// Standardausgabe
		System.out.printf( "\n|%s|\n", s ); 	// |Java-Programm| 
		
		// rechtsb�ndig mit 20 Stellen
		System.out.printf( "|%20s|\n", s );     // |       Java-Programm|
		
		// linksb�ndig mit 20 Stellen
		System.out.printf( "|%-20s|\n", s );    // |Java-Programm       |
		
		// minimal 5 stellen
		System.out.printf( "|%5s|\n", s );      // |Java-Programm|
		
		// maximal 4 Stellen
		System.out.printf( "|%.4s|\n", s );     // |Java|
		
		// 20 Positionen, rechtsb�ndig, h�chstens 4 Stellen von String
		System.out.printf( "|%20.4s|", s );   // |                Java|
		
		
		System.out.println("\n");
		
	
int i = 123;
		
		// rechtsb�ndig (Stellen nach Bedarf)
		System.out.printf( "|%d|   |%d|\n" ,     i, -i);    // |123|   |-123|
		
		// rechtsb�ndig (mindestens 5 Stellen)
		System.out.printf( "|%5d| |%5d|\n" ,     i, -i);    // |  123| |  -123|
		
		// linksb�ndig (mindestens 5 Stellen)
		System.out.printf( "|%-5d| |%-5d|\n" ,   i, -i);    // |123  | |-123 |

		// zus�tzlich mit Vorzeichen (auch positive Zahlen)
		System.out.printf( "|%+-5d| |%+-5d|\n" , i, -i);    // |+123 | |-123 |
		
		// Auff�llen nach vorne mit Nullen
		System.out.printf( "|%05d| |%05d|\n\n",  i, -i);    // |00123| |-0123|
		
		// hexadezimale Darstellung in Gro�- bzw. Kleinbuchstaben
		System.out.printf( "|%X| |%x|\n", 0xabc, 0xabc );   // |ABC| |abc|
		
		// hexadezimale Darstellung mit Null-Auff�llung und x in der Anzeige
		System.out.printf( "|%08x| |%#x|", 0xabc, 0xabc ); // |00000abc| |0xabc|
		
		
		System.out.println("\n");
		

double d = 1234.5678;

		// Standard-Ausgabe von Dezimalzahlen mit 6 Nachkommastellen
		System.out.printf( "|%f| |%f|\n" ,         d, -d); 
		// |1234,567800| |-1234,567800|
		// 2 Nachkommastellen
		System.out.printf( "|%.2f| |%.2f|\n" ,     d, -d); 
		// |1234,57| |-1234,57|
		// 10 Stellen insgesamt
		System.out.printf( "|%10f| |%10f|\n" ,     d, -d); 
		// |1234,567800| |-1234,567800|
		// 10 Stellen gesamt, davon 2 Nachkommastellen
		System.out.printf( "|%10.2f| |%10.2f|\n" , d, -d); 
		// |   1234,57| |   1234,57|
		// zus�tzlich Auff�llen mit Nullen
		System.out.printf( "|%010.2f| |%010.2f|\n", d, -d); 
		// |0001234,57| |-001234,57|

		
	}

}
